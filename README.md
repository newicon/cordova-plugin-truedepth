# cordova-plugin-truedepth

Cordova plugin using ARKit(iOS) and ARCore(Android) to calculate distance of real world face measurements

Swift(iOS) inspired by: https://github.com/levantAJ/Measure
Kotlin(Android) inspired by: https://github.com/Terran-Marine/ARCoreMeasuredDistance

## Supported Platforms

- iOS (Requires ARKit support iOS 11.3+)
- Android (See devices supported: https://developers.google.com/ar/discover/supported-devices)

## Dependencies

- cordova-plugin-add-swift-support ~2

## Installation
    (Not yet available) cordova plugin add cordova-plugin-truedepth

## Methods
- cordova.plugins.measure.start
- cordova.plugins.measure.onMeasureUpdate
- cordova.plugins.measure.onFinish

----

### start
Insert the camera view under the WebView

##### Parameters

```js
cordova.plugins.measure.start();
```

----

### AR Mask View
Insert an ARview that uses the front camera and displays a mask in AR

```js
cordova.plugins.measure.mask()
```

### onMeasureUpdate

Set listener for event from measures

```js
cordova.plugins.measure.onMeasureUpdate((data) => {});

// Example: data = {"10.00cm"}
```

----

### onFinish

Set listener when the view is dismissed

```js
cordova.plugins.measure.onFinish((data) => {});

/*
Examples:

IF allowMultiplePoints == FALSE:
data = "10.00cm"

IF allowMultiplePoints == TRUE:
data = ["10.00cm", "20.14cm", ...]
*/
```
