//
//  ViewController.swift
//  Measure
//
//  Created by Neil Clayton 
//

import UIKit
import SceneKit
import ARKit

@objc protocol ViewControllerDelegate: class {
    func allowMultiple() -> Bool
    func closeView()
    func onUpdateMeasure(nodeName: String)
}

final class ViewController: UIViewController {
    @IBOutlet weak var sceneView: ARSCNView!
    private var startCapture = false
    private var counter = 0
    private var TestLimiter = 400
    
    
    
    @IBOutlet weak var targetImageView: UIImageView!
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var meterImageView: UIImageView!
    @IBOutlet weak var resetImageView: UIImageView!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    
    fileprivate lazy var session = ARSession()
    fileprivate lazy var sessionConfiguration = ARWorldTrackingConfiguration()
    fileprivate lazy var isMeasuring = false;
    fileprivate lazy var vectorZero = SCNVector3()
    fileprivate lazy var startValue = SCNVector3()
    fileprivate lazy var endValue = SCNVector3()
    fileprivate lazy var lines: [Line] = []
    fileprivate var currentLine: Line?
    fileprivate lazy var unit: DistanceUnit = .centimeter

    func getMeasures() -> [String] {
        var list: [String] = [];

        for line in lines {
            list.append(line.getValue());
        }

        return list;
    }
    
    /// Delegate
    var delegate: ViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard ARFaceTrackingConfiguration.isSupported else {
            fatalError("Face tracking is not support on this device")
        }
        
        sceneView.delegate = self
        
        let btnCapture = UIButton(type: .custom)
        btnCapture.setTitle("Submit", for: .normal)
        btnCapture.tintColor = .black
        btnCapture.titleLabel?.textColor = .black
        btnCapture.backgroundColor = .lightGray
        btnCapture.addTarget(self, action: #selector(capture), for: .touchUpInside)
        btnCapture.layer.cornerRadius = 8
        view.addSubview(btnCapture)
        
        btnCapture.translatesAutoresizingMaskIntoConstraints = false
        let constraints: [NSLayoutConstraint] = [
            btnCapture.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            btnCapture.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -40),
            btnCapture.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            btnCapture.heightAnchor.constraint(equalToConstant: 55)
        ]
        view.addConstraints(constraints)
        
        
        // setupScene()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let configuration = ARFaceTrackingConfiguration()
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()

        //session.pause()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {}
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {}
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    @objc
    private func capture() {
        startCapture = true
    }
}



extension ViewController: ARSCNViewDelegate {
    
    func renderer(_ renderer: SCNSceneRenderer,
                  nodeFor anchor: ARAnchor) -> SCNNode? {
        guard let device = sceneView.device else {
            return nil
        }
        
     
        let faceGeometry = ARSCNFaceGeometry(device: device)
        let node = SCNNode(geometry: faceGeometry)
        
        //uncomment array and use in the following for in loop to show label of all vertices
        //let values = Array(1...1220)
        //[145, 133, 160, 371, 367, 387, 14, 818, 798, 802,  609, 582, 594] NOSE bridge
        
        for x in [20, 956, 1022, 1041, 19, 18, 17, 16, 15 , 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 0, 1, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 975, 1049, 1048] {
            
            
            let text = SCNText(string: "\(x)", extrusionDepth: 1)
            text.firstMaterial?.diffuse.contents = UIColor.blue
            let txtnode = SCNNode(geometry: text)
            txtnode.scale = SCNVector3(x: 0.0002, y: 0.0002, z: 0.0002)
            txtnode.name = "\(x)"
            node.addChildNode(txtnode)
            
            txtnode.geometry?.firstMaterial?.fillMode = .fill
        }
        node.geometry?.firstMaterial?.fillMode = .lines
     
        return node
    }
     
    func renderer(_ renderer: SCNSceneRenderer,
                  didUpdate node: SCNNode,
                  for anchor: ARAnchor) {
       
        guard let faceAnchor = anchor as? ARFaceAnchor,
              let faceGeometry = node.geometry as? ARSCNFaceGeometry
            else {
                return
            }
        
        print(faceAnchor.transform)
        if(startCapture){
            if(counter < TestLimiter){
                counter += 1
                    for x in 0..<1220 {
                        let child = node.childNode(withName: "\(x)", recursively: false)
                        child?.position = SCNVector3(faceAnchor.geometry.vertices[x])
                    }
            
                    
                    //let nose_array:Array = [145, 133, 160, 371, 367, 387, 14, 818, 798, 802,  609, 582, 594]
                    //let nose2chin_array:Array = [3, 2, 0, 1, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 975, 1049, 1048]
                    let fullface:Array = [20, 956, 1022, 1041, 19, 18, 17, 16, 15 , 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 0, 1, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 975, 1049, 1048]
                    var distance:Float = 0.0
                    for (x, element) in fullface.enumerated(){
                        if(x+1 < fullface.count){
                            let dist = simd_distance(faceAnchor.geometry.vertices[element], faceAnchor.geometry.vertices[fullface[x+1]])
                        
                            distance = distance + dist
                        }
                    }
                
                
                    print("\(distance*1000),")
            }
            
        }
        
        faceGeometry.update(from: faceAnchor.geometry)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {}
    
    func session(_ session: ARSession, didFailWithError error: Error) {}
    
    func sessionWasInterrupted(_ session: ARSession) {}
    
    func sessionInterruptionEnded(_ session: ARSession) {}
}



extension ViewController {
    @IBAction func meterButtonTapped(button: UIButton) {
        let alertVC = UIAlertController(title: "Settings", message: "Please select distance unit options", preferredStyle: .actionSheet)
        alertVC.addAction(UIAlertAction(title: DistanceUnit.centimeter.title, style: .default) { [weak self] _ in
            self?.unit = .centimeter
        })
        alertVC.addAction(UIAlertAction(title: DistanceUnit.inch.title, style: .default) { [weak self] _ in
            self?.unit = .inch
        })
        alertVC.addAction(UIAlertAction(title: DistanceUnit.meter.title, style: .default) { [weak self] _ in
            self?.unit = .meter
        })
        alertVC.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alertVC, animated: true, completion: nil)
    }
    
    @IBAction func resetButtonTapped(button: UIButton) {}
    
    @IBAction func closeButtonTapped(button: UIButton) {
        delegate?.closeView();
    }
}


extension ViewController {
    fileprivate func setupScene() {}
    
    fileprivate func resetValues() {}
    
    fileprivate func detectObjects() {}
}
