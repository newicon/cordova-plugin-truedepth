//
//  ViewController.swift
//  Measure
//
//  Created by Neil Clayton 
//

import UIKit
import SceneKit
import ARKit

@objc protocol ARViewControllerDelegate: class {
    func allowMultiple() -> Bool
    func closeView()
    func onUpdateMeasure(nodeName: String)
}

final class ARViewController: UIViewController {
    @IBOutlet weak var sceneView: ARSCNView!
       var contentNode: SCNNode?
    var occlusionNode: SCNNode!
    private var startCapture = false
    private var counter = 0
    private var TestLimiter = 400
    
    
    
    @IBOutlet weak var targetImageView: UIImageView!
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var meterImageView: UIImageView!
    @IBOutlet weak var resetImageView: UIImageView!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    
    fileprivate lazy var session = ARSession()
    fileprivate lazy var sessionConfiguration = ARWorldTrackingConfiguration()
    fileprivate lazy var isMeasuring = false;
    fileprivate lazy var vectorZero = SCNVector3()
    fileprivate lazy var startValue = SCNVector3()
    fileprivate lazy var endValue = SCNVector3()
    fileprivate lazy var lines: [Line] = []
    fileprivate var currentLine: Line?
    fileprivate lazy var unit: DistanceUnit = .centimeter

    func getMeasures() -> [String] {
        var list: [String] = [];

        for line in lines {
            list.append(line.getValue());
        }

        return list;
    }
    
    /// Delegate
    var delegate: ARViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard ARFaceTrackingConfiguration.isSupported else {
            fatalError("Face tracking is not support on this device")
        }
        
        sceneView.delegate = self
        
        let btnCapture = UIButton(type: .custom)
        btnCapture.setTitle("Submit", for: .normal)
        btnCapture.tintColor = .black
        btnCapture.titleLabel?.textColor = .black
        btnCapture.backgroundColor = .lightGray
        btnCapture.addTarget(self, action: #selector(capture), for: .touchUpInside)
        btnCapture.layer.cornerRadius = 8
        view.addSubview(btnCapture)
        
        btnCapture.translatesAutoresizingMaskIntoConstraints = false
        let constraints: [NSLayoutConstraint] = [
            btnCapture.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            btnCapture.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -40),
            btnCapture.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            btnCapture.heightAnchor.constraint(equalToConstant: 55)
        ]
        view.addConstraints(constraints)
        
        
        // setupScene()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let configuration = ARFaceTrackingConfiguration()
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()

        //session.pause()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {}
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {}
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    @objc
    private func capture() {
        startCapture = true
    }
}

extension SCNReferenceNode {
    convenience init(named resourceName: String, loadImmediately: Bool = true) {
        let url = Bundle.main.url(forResource: resourceName, withExtension: "scn")!
        self.init(url: url)!
        if loadImmediately {
            self.load()
        }
    }
}


extension ARViewController: ARSCNViewDelegate {

        func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
            guard let sceneView = renderer as? ARSCNView,
                anchor is ARFaceAnchor else { return nil }

            #if targetEnvironment(simulator)
            #error("ARKit is not supported in iOS Simulator. Connect a physical iOS device and select it as your Xcode run destination, or select Generic iOS Device as a build-only destination.")
            #else
  
            let faceGeometry = ARSCNFaceGeometry(device: sceneView.device!)!
            faceGeometry.firstMaterial!.colorBufferWriteMask = []
            occlusionNode = SCNNode(geometry: faceGeometry)
            occlusionNode.renderingOrder = -1


            let faceOverlayContent = SCNReferenceNode(named: "Models.scnassets/mask")

            contentNode = SCNNode()
            contentNode!.addChildNode(occlusionNode)
            contentNode!.addChildNode(faceOverlayContent)
            #endif
            return contentNode
        }
        
        func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
            guard let faceGeometry = occlusionNode.geometry as? ARSCNFaceGeometry,
                let faceAnchor = anchor as? ARFaceAnchor
                else { return }
            
            faceGeometry.update(from: faceAnchor.geometry)
        }
}




// extension ViewController {
//     @IBAction func meterButtonTapped(button: UIButton) {
//         let alertVC = UIAlertController(title: "Settings", message: "Please select distance unit options", preferredStyle: .actionSheet)
//         alertVC.addAction(UIAlertAction(title: DistanceUnit.centimeter.title, style: .default) { [weak self] _ in
//             self?.unit = .centimeter
//         })
//         alertVC.addAction(UIAlertAction(title: DistanceUnit.inch.title, style: .default) { [weak self] _ in
//             self?.unit = .inch
//         })
//         alertVC.addAction(UIAlertAction(title: DistanceUnit.meter.title, style: .default) { [weak self] _ in
//             self?.unit = .meter
//         })
//         alertVC.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//         present(alertVC, animated: true, completion: nil)
//     }
    
//     @IBAction func resetButtonTapped(button: UIButton) {}
    
//     @IBAction func closeButtonTapped(button: UIButton) {
//         delegate?.closeView();
//     }
// }


// extension ViewController {
//     fileprivate func setupScene() {}
    
//     fileprivate func resetValues() {}
    
//     fileprivate func detectObjects() {}
// }

// func distanceUnit(x1, x2){
//     return (x1 - x2) ^ 2
// }