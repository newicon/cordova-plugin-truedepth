import SceneKit

@objc(HWPMeasurePlugin) class MeasurePlugin : CDVPlugin {
    var allowMultiplePoints: Bool!;

    var measureListenerCallbackId: String!
    var finishListenerCallbackId: String!
    
    var myViewController: ViewController!
    var myARViewController: ARViewController!
    
    @objc func addARView(_ command: CDVInvokedUrlCommand) {
        DispatchQueue.global(qos: .utility).async {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let myViewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as? ViewController else {
                fatalError("ViewController is not set in storyboard")
            }
            self.myViewController = myViewController
            //self.myViewController.delegate = self

            DispatchQueue.main.async {
                let options = command.arguments[0] as! NSMutableDictionary
                let multiplePointsEnabled = options.object(forKey: "allowMultiplePoints") as! Bool;

                self.allowMultiplePoints = multiplePointsEnabled

                guard let superview = self.webView.superview else { return }
                self.webView.isOpaque = false;
                self.webView.backgroundColor = UIColor.clear;
                superview.insertSubview(self.myViewController.view, belowSubview: self.webView)
            }
        }
    }
    @objc func maskARView(_ command: CDVInvokedUrlCommand) {
        DispatchQueue.global(qos: .utility).async {
            let storyboard = UIStoryboard(name: "ARMain", bundle: nil)
            guard let myViewController = storyboard.instantiateViewController(withIdentifier: "ARViewController") as? ARViewController else {
                fatalError("ViewController is not set in storyboard")
            }
            self.myARViewController = myViewController
            //self.myViewController.delegate = self

            DispatchQueue.main.async {
                let options = command.arguments[0] as! NSMutableDictionary
                let multiplePointsEnabled = options.object(forKey: "allowMultiplePoints") as! Bool;

                self.allowMultiplePoints = multiplePointsEnabled

                guard let superview = self.webView.superview else { return }
                self.webView.isOpaque = false;
                self.webView.backgroundColor = UIColor.clear;
                superview.insertSubview(self.myARViewController.view, belowSubview: self.webView)
            }
        }
    }
}
